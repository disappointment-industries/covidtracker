import 'package:covidtracker/CountryPage.dart';
import 'package:covidtracker/MainPage.dart';
import 'package:covidtracker/Models.dart';
import 'package:covidtracker/country_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';



class FormattedNumber extends StatelessWidget {
  final num number;
  FormattedNumber(this.number, {Key? key}): super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      number.isFinite ? NumberFormat().format(number.round()) : "N/A",
      textAlign: TextAlign.end,
    );
  }
}

typedef String StringLocalizer(BuildContext context);

Map<StringLocalizer, Tuple2<ColumnSelector, TextAlign>> _rows = {
  (context) => AppLocalizations.of(context)!.countryName: Tuple2(
    (country) => country.country,
    TextAlign.start
  ),

  (context) => AppLocalizations.of(context)!.countryCases: Tuple2(
    (country) => country.cases,
    TextAlign.end
  ),

  (context) => AppLocalizations.of(context)!.countryDeaths: Tuple2(
    (country) => country.deaths,
    TextAlign.end,
  ),

  (context) => AppLocalizations.of(context)!.countryRecovered: Tuple2(
    (country) => country.recovered,
    TextAlign.end
  ),
};

class CountryRowBuilder {
  Row header() {
    return Row(
      children: _rows.entries.map((e) => Expanded(
        child: Consumer<MainModel>(
          builder: (context, model, _) {
            return InkWell(
              child: Text(
                e.key(context),
                textAlign: e.value.item2
              ),
              onTap: (){
                if (model.selector == e.value.item1) {
                  model.descending = !model.descending;
                }
                model.selector = e.value.item1;
              },
            );
          },
        ),
      )).toList(),
    );
  }

  Widget row(Country c) {
    return Selector<MainModel, bool>(
      selector: (_, model) => model.perMillion,
      builder: (context, perMill, _) {
        var div = 1.0;
        if (perMill) {
          div = c.mils;
        }
        return Row(
          children: [
            Expanded(child: Text(c.country, key: UniqueKey(),)),
            Expanded(child: FormattedNumber(c.cases / div, key: UniqueKey())),
            Expanded(child: FormattedNumber(c.deaths / div, key: UniqueKey())),
            Expanded(child: FormattedNumber(c.recovered / div, key: UniqueKey())),
          ],
        );
      },
    );
  }
}
