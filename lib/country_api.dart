import 'dart:convert';

import 'package:json_annotation/json_annotation.dart';
import 'package:http/http.dart' as http;

// flutter pub run build_runner build
part 'country_api.g.dart';

const _url = "https://disease.sh/v3/covid-19/countries?yesterday=true";

@JsonSerializable()
class CountryInfo {
  final String? iso3;
  final String? flag;

  CountryInfo({required this.iso3, this.flag});

  factory CountryInfo.fromJson(Map<String, dynamic> json) => _$CountryInfoFromJson(json);

  Map<String, dynamic> toJson() => _$CountryInfoToJson(this);
}

@JsonSerializable()
class Country {
  final String country;
  final int population;
  final int cases;
  final int todayCases;
  final int deaths;
  final int todayDeaths;
  final int recovered;
  final int todayRecovered;
  final CountryInfo countryInfo;

  double get mils => population/1e6;

  Country({
    required this.country,
    required this.population,
    required this.cases,
    required this.todayCases,
    required this.deaths,
    required this.todayDeaths,
    required this.recovered,
    required this.todayRecovered,
    required this.countryInfo,
  });

  factory Country.fromJson(Map<String, dynamic> json) => _$CountryFromJson(json);

  Map<String, dynamic> toJson() => _$CountryToJson(this);
}

Future<List<Country>> getCountries() async {
  var resp = await http.get(Uri.parse(_url));
  var list = jsonDecode(resp.body) as List<dynamic>;
  var countries = list.map((e) => Country.fromJson(e as Map<String, dynamic>)).toList();
  return countries;
}

