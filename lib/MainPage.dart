import 'package:covidtracker/Counter.dart';
import 'package:covidtracker/CountryList.dart';
import 'package:covidtracker/CountryPage.dart';
import 'package:covidtracker/LineGraph.dart';
import 'package:covidtracker/LoadingScreen.dart';
import 'package:covidtracker/Models.dart';
import 'package:covidtracker/SummaryCard.dart';
import 'package:covidtracker/country_api.dart';
import 'package:covidtracker/sum_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';
import 'dart:math';

import 'package:sliver_tools/sliver_tools.dart';

class MainView extends StatelessWidget {

  final CountryRowBuilder c = CountryRowBuilder();

  @override
  Widget build(BuildContext context) {
    return LoadingScreen<MainModel>(
      child: CustomScrollView(
        slivers: <Widget>[
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Selector<MainModel, WeekData?>(
                      selector: (context, model) => model.sum,
                      builder: (context, sum, _) {
                        if (sum == null) {
                          return Row();
                        }
                        return SummaryCard(AppLocalizations.of(context)!.worldWide, sum);
                      }
                  )
                ],
              ),
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.all(20),
            sliver: MultiSliver(
              children: [
                SliverStack(
                  insetOnOverlap: true,
                  children: [
                    SliverPositioned.fill(
                      child: _CardBackground(),
                    ),
                    MultiSliver(
                      children: [
                        SliverPinnedHeader(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Theme.of(context).backgroundColor,
                              boxShadow: const <BoxShadow>[
                                BoxShadow(
                                  offset: Offset(0, 4),
                                  blurRadius: 8,
                                  color: Colors.black26,
                                )
                              ],
                              borderRadius: BorderRadius.circular(8),
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(20),
                              child: c.header(),
                            ),
                          )
                        ),
                        SliverPadding(
                          padding: EdgeInsets.only(
                            top: 20,
                            bottom: 20,
                          ),
                          sliver: Selector<MainModel, int>(
                            selector: (_, model) => model.countries?.length ?? 0,
                            builder: (context, count, _) {
                              return SliverList(
                                delegate: SliverChildBuilderDelegate(
                                  (BuildContext context, int index) {
                                    return Selector<MainModel, Country>(
                                      selector: (_, model) => model.countries![index],
                                      builder: (context, country, _) {
                                        return InkWell(
                                          onTap: (){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context){
                                                return CountryPage(country.countryInfo.iso3 ?? "", country.country);
                                              })
                                            );
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              color: index % 2 == 0 ? null : Theme.of(context).backgroundColor,
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                top: 10,
                                                bottom: 10,
                                                left: 20,
                                                right: 20
                                              ),
                                              child: c.row(country),
                                            ),
                                          ),
                                        );
                                      },
                                    );
                                  },
                                  childCount: count,
                                )
                              );
                            },
                          ),
                        ),
                      ]
                    ),
                  ]
                ),
              ]
            ),
          ),
        ],
      ),
    );
  }
}

class _CardBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        boxShadow: const <BoxShadow>[
          BoxShadow(
            offset: Offset(0, 4),
            blurRadius: 8,
            color: Colors.black26,
          )
        ],
        borderRadius: BorderRadius.circular(8),
      ),
    );
  }
}