import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class Counter extends StatelessWidget {
  final String name;
  final num count;

  Counter(this.name, this.count);

  @override
  Widget build(BuildContext context) {
    return Wrap(
      direction: Axis.vertical,
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints){
            var style = Theme.of(context).textTheme.headline4;
            if (MediaQuery.of(context).size.width < 700) {
              style = Theme.of(context).textTheme.headline5;
            }
            return Text(
              "${NumberFormat().format(count)}",
              style: style,
            );
          },
        ),
        Text(
          name
        )
      ],
    );
  }
}