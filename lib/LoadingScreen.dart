import 'package:covidtracker/sum_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


import 'Models.dart';

class LoadingScreen<T extends Model> extends StatelessWidget {
  final Widget child;

  LoadingScreen({required this.child});

  final switchKey = UniqueKey();

  @override
  Widget build(BuildContext context) {
    return Selector<T, Tuple2<bool, Object?>>(
      selector: (context, model) => Tuple2(model.loaded, model.error),
      builder: (context, state, _) {
        late Widget view;
        if (state.item2 != null) { // error
          var errorStr = state.item2.toString();
          if (state.item2 is ApiError) {
            var e = state.item2 as ApiError;
            errorStr = e.localizedString(context);
          }
          view = Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(Icons.error),
                Text(AppLocalizations.of(context)!.error+":"),
                Text(errorStr)
              ],
            )
          );
        } else if (state.item1) { // loaded
          view = child;
        } else { // loading
          view = Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                CircularProgressIndicator()
              ],
            ),
          );
        }
        return AnimatedSwitcher(
          key: switchKey,
          duration: Duration(seconds: 1),
          child: view,
        );
      },
    );
  }
}