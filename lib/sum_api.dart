import 'dart:convert';

import 'package:covidtracker/MainPage.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'package:json_annotation/json_annotation.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';


// flutter pub run build_runner build
part 'sum_api.g.dart';

const allUrl = "https://disease.sh/v3/covid-19/historical/all";
const countryUrl = "https://disease.sh/v3/covid-19/historical";


extension ToSpots on Map<String, int> {
  List<FlSpot> toSpots() {
    List<FlSpot> data = [];
    forEach((key, value) {
      var parts = key.split("/");
      var date = new DateTime(int.parse(parts[2]), int.parse(parts[0]), int.parse(parts[1]));
      data.add(FlSpot(date.millisecondsSinceEpoch.toDouble(), value.toDouble()));
    });
    return data;
  }
}

@JsonSerializable()
class WeekData {
  WeekData(this.cases, this.deaths);
  final Map<String, int> cases;
  final Map<String, int> deaths;

  factory WeekData.fromJson(Map<String, dynamic> json) => _$WeekDataFromJson(json);

  Map<String, dynamic> toJson() => _$WeekDataToJson(this);
}

Future<WeekData> getWeekAll({int n = 7}) async {
  var url = Uri.parse("$allUrl?lastdays=$n");
  var response = await http.get(url);
  WeekData data = WeekData.fromJson(jsonDecode(response.body));
  return data;
}

extension MapDelta on Map<String, int> {
  Map<String, int> deltas() {
    int? last;
    Map<String, int> resp = {};
    this.forEach((key, value) {
      if (last != null) {
        resp[key] = value-last!;
      }
      last = value;
    });
    return resp;
  }
}

enum ApiError {
  NoSuchCountry,
  NoDataForCountry
}


extension LocalizedString on ApiError {
  String localizedString(BuildContext context) {
    switch(this){
      case ApiError.NoSuchCountry:
        return AppLocalizations.of(context)!.noSuchCountry;
      case ApiError.NoDataForCountry:
        return AppLocalizations.of(context)!.noDataForCountry;
    }
  }
}

Future<WeekData> getWeekCountry(String country, {int n = 7}) async {
  var url = Uri.parse("$countryUrl/$country?lastdays=$n");
  var response = await http.get(url);
  var dec = jsonDecode(response.body);
  if (dec == null) {
    throw(ApiError.NoSuchCountry);
  }
  var tmp = dec as Map<String, dynamic>;
  if (tmp["timeline"] == null) {
    throw(ApiError.NoDataForCountry);
  }
  WeekData data = WeekData.fromJson(tmp["timeline"] as Map<String, dynamic>);
  return data;
}