
import 'dart:math';

import 'package:covidtracker/Counter.dart';
import 'package:covidtracker/sum_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'LineGraph.dart';


class SummaryCard extends StatelessWidget {

  final WeekData data;

  final String title;

  SummaryCard(this.title, this.data);

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              title,
              style: Theme
                  .of(context)
                  .textTheme
                  .headline5,
            ),
            LineGraph(
              [
                GraphLine(
                    spots: data.cases.deltas().toSpots(),
                    name: AppLocalizations.of(context)!
                        .dailyCases,
                    color: Colors.blue
                ),
              ],
              key: UniqueKey(),
            ),
            LineGraph(
              [
                GraphLine(
                    spots: data.deaths.deltas().toSpots(),
                    name: AppLocalizations.of(context)!
                        .dailyDeaths,
                    color: Colors.red
                )
              ],
              key: UniqueKey(),
            ),
            Padding(
              padding: EdgeInsets.only(top: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Counter(
                      AppLocalizations.of(context)!.totalCases,
                      data.cases.values.reduce(max)),
                  Counter(
                      AppLocalizations.of(context)!.totalDeaths,
                      data.deaths.values.reduce(max)),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}