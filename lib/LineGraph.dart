import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:tuple/tuple.dart';

class GraphLine {
  final List<FlSpot> spots;
  final String name;
  final Color color;

  GraphLine({required this.spots, required this.name, required this.color});
}

class LineGraph extends StatelessWidget {

  final List<GraphLine> lines;
  late final Tuple2<double, double> _intervals;
  late final Tuple2<double, double> _minMax;

  LineGraph(this.lines, {Key? key}): super(key: key) {
    var meta = _findInterval();
    _intervals = meta.item1;
    _minMax = meta.item2;
  }

  Tuple2<Tuple2<double, double>, Tuple2<double, double>> _findInterval() {
    double intervX = double.infinity;
    double intervY = double.infinity;

    Tuple2<double, double> minMax = Tuple2(double.infinity, -double.infinity);

    lines.forEach((line) {
      Tuple2<double, double> x = Tuple2(double.infinity, -double.infinity);
      Tuple2<double, double> y = Tuple2(double.infinity, -double.infinity);
      line.spots.forEach((spot){
        if (spot.x < x.item1) {
          x = x.withItem1(spot.x);
        }
        if (spot.x > x.item2) {
          x = x.withItem2(spot.x);
        }

        if (spot.y < y.item1) {
          y = y.withItem1(spot.y);
        }
        if (spot.y > y.item2) {
          y = y.withItem2(spot.y);
        }
      });
      double intX = (x.item2-x.item1)/(line.spots.length-1);
      double intY = (y.item2-y.item1)/7.01;
      if (intX < intervX) {
        intervX = intX;
      }
      if (intY < intervY) {
        intervY = intY;
      }

      if (y.item1 < minMax.item1) {
        minMax = minMax.withItem1(y.item1);
      }
      if (y.item2 > minMax.item2) {
        minMax = minMax.withItem2(y.item2);
      }
    });

    return Tuple2(Tuple2(intervX, intervY), minMax);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: [
          Container(
            height: 200,
            child: Padding(
                padding: const EdgeInsets.only(
                    top: 60,
                    left: 28,
                    right: 50,
                ),
                child: LayoutBuilder(
                  builder: (BuildContext context, BoxConstraints constraints) {
                    var fmt = DateFormat("EEE");
                    if (constraints.maxWidth > 500) {
                      fmt = DateFormat("EEEE");
                    }

                    return LineChart(
                        LineChartData(
                          minY: _minMax.item1 - (_minMax.item2 - _minMax.item1)*.15,
                          maxY: _minMax.item2 + (_minMax.item2 - _minMax.item1)*.15,
                          lineBarsData: lines.map((e) {
                            return LineChartBarData(
                                isCurved: true,
                                spots: e.spots,
                                colors: [e.color]
                            );
                          }).toList(),
                          titlesData: FlTitlesData(
                            leftTitles: SideTitles(
                                showTitles: true,
                                interval: _intervals.item2,
                                getTextStyles: (v) {
                                  return Theme.of(context).textTheme.bodyText1!;
                                },
                                reservedSize: 50
                            ),
                            bottomTitles: SideTitles(
                                getTextStyles: (v) {
                                  return Theme.of(context).textTheme.bodyText1!;
                                },
                                interval: _intervals.item1,
                                showTitles: true,
                                getTitles: (value) {
                                  var date = DateTime.fromMillisecondsSinceEpoch(value.floor());
                                  return fmt.format(date);
                                }
                            ),
                          ),
                          lineTouchData: LineTouchData(
                            touchTooltipData: LineTouchTooltipData(
                              tooltipBgColor: Theme.of(context).backgroundColor,
                              getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
                                return touchedBarSpots.map((e) {
                                  return LineTooltipItem("${NumberFormat().format(e.y)}", Theme.of(context).textTheme.bodyText1!);
                                }).toList();
                              }
                            ),
                          ),
                        )
                    );
                  },
                )
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: lines.map((e) {
              return Wrap(
                alignment: WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  Container(
                    width: 5,
                    height: 5,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: e.color,
                    ),
                  ),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(e.name)
                ],
              );
            }).toList(),
          ),
        ]
    );
  }
}