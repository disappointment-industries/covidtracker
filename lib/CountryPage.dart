import 'package:covidtracker/LoadingScreen.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

import 'package:covidtracker/SummaryCard.dart';
import 'package:covidtracker/sum_api.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'Models.dart';

class CountryPage extends StatelessWidget {

  final String country;
  final String iso;

  CountryPage(this.iso, this.country);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(country),
      ),
      body: CountryView(iso, country),
    );
  }
}

class CountryView extends StatelessWidget {
  final String country;
  final String iso;

  CountryView(this.iso, this.country);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CountryModel(iso),
      child: LoadingScreen<CountryModel>(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Selector<CountryModel, WeekData?>(
                selector: (_, model) => model.data,
                builder: (context, data, _) {
                  return SummaryCard(country, data!);
                },
              ),
            )
          ]
        ),
      )
    );
  }
}
