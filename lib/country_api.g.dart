// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'country_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CountryInfo _$CountryInfoFromJson(Map<String, dynamic> json) {
  return CountryInfo(
    iso3: json['iso3'] as String?,
    flag: json['flag'] as String?,
  );
}

Map<String, dynamic> _$CountryInfoToJson(CountryInfo instance) =>
    <String, dynamic>{
      'iso3': instance.iso3,
      'flag': instance.flag,
    };

Country _$CountryFromJson(Map<String, dynamic> json) {
  return Country(
    country: json['country'] as String,
    population: json['population'] as int,
    cases: json['cases'] as int,
    todayCases: json['todayCases'] as int,
    deaths: json['deaths'] as int,
    todayDeaths: json['todayDeaths'] as int,
    recovered: json['recovered'] as int,
    todayRecovered: json['todayRecovered'] as int,
    countryInfo:
        CountryInfo.fromJson(json['countryInfo'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$CountryToJson(Country instance) => <String, dynamic>{
      'country': instance.country,
      'population': instance.population,
      'cases': instance.cases,
      'todayCases': instance.todayCases,
      'deaths': instance.deaths,
      'todayDeaths': instance.todayDeaths,
      'recovered': instance.recovered,
      'todayRecovered': instance.todayRecovered,
      'countryInfo': instance.countryInfo,
    };
