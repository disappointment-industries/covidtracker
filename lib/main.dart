import 'package:covidtracker/MainPage.dart';
import 'package:covidtracker/Models.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Covid Tracker',
      localizationsDelegates: [
        AppLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: [
        const Locale("en", ""),
        const Locale("hu", "")
      ],
      theme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        primarySwatch: Colors.blue,
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.system,
      home: ChangeNotifierProvider(
        create: (_) => MainModel(),
        child: Scaffold(
          appBar: AppBar(
            title: Text("CovidTracker"),
            actions: [
              Consumer<MainModel>(
                builder: (context, model, _) {
                  return Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Row(
                      children: [
                        Text(AppLocalizations.of(context)!.perMillion),
                        Checkbox(
                          value: model.perMillion,
                          onChanged: (bool? value) {
                            model.perMillion = value ?? false;
                          }
                        )
                      ],
                    ),
                  );
                },
              )
            ],
          ),
          body: MainView(),
        ),
      ),
    );
  }
}