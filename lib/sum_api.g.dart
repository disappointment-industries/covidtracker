// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sum_api.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WeekData _$WeekDataFromJson(Map<String, dynamic> json) {
  return WeekData(
    Map<String, int>.from(json['cases'] as Map),
    Map<String, int>.from(json['deaths'] as Map),
  );
}

Map<String, dynamic> _$WeekDataToJson(WeekData instance) => <String, dynamic>{
      'cases': instance.cases,
      'deaths': instance.deaths,
    };
