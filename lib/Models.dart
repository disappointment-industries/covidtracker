import 'package:covidtracker/CountryList.dart';
import 'package:covidtracker/country_api.dart';
import 'package:covidtracker/sum_api.dart';
import 'package:flutter/cupertino.dart';


abstract class Model extends ChangeNotifier {
  void update();
  Object? _error;
  Object? get error => _error;

  bool get loaded;

  Model() {
    update();
  }
}

typedef Comparable ColumnSelector(Country country);

class MainModel extends Model {
  WeekData? _sum;
  List<Country>? _countries;

  WeekData? get sum => _sum;
  List<Country>? get countries => _countries;

  bool get loaded => sum != null && countries != null;

  bool _perMillion = false;
  bool get perMillion => _perMillion;
  set perMillion(bool perMill) {
    _perMillion = perMill;
    sort();
  }

  bool _descending = false;
  bool get descending => _descending;
  set descending(bool asc) {
    _descending = asc;
    sort();
  }

  ColumnSelector _selector = (Country c) => c.country;
  ColumnSelector get selector => _selector;
  set selector(ColumnSelector sel) {
    _selector = sel;
    sort();
  }

  void sort() {
    _countries!.sort((a, b) {
      if (perMillion && a.population == 0) {
        return 1;
      }
      if (perMillion && b.population == 0) {
        return -1;
      }
      var fieldA = selector(a);
      var fieldB = selector(b);
      if (perMillion && fieldA is int && fieldB is int) {
        fieldA /= a.population;
        fieldB /= b.population;
      }
      var comp = fieldA.compareTo(fieldB);
      if (descending) {
        comp *= -1;
      }
      return comp;
    });
    
    notifyListeners();
  }

  MainModel(){
    update();
  }

  void update() async {
    try {
      var _all = getWeekAll();
      var _cntries = getCountries();
      var results = await Future.wait([_all, _cntries]);

      _sum = results[0] as WeekData;
      _countries = results[1] as List<Country>;
    } catch (e) {
      _error = e;
    }
    notifyListeners();
  }
}

class CountryModel extends Model {
  WeekData? _data;
  WeekData? get data => _data;

  final String iso;

  bool get loaded => data != null;

  CountryModel(this.iso) {
    update();
  }

  void update() async {
    try {
      _data = await getWeekCountry(iso);
    } catch (e) {
      _error = e;
    }
    notifyListeners();
  }
}